package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (!isValid(start, end)) return null;
        return generateTable(start, end);
    }

    public Boolean isValid(int start, int end) {
        return isStartNotBiggerThanEnd(start, end) && isInRange(start) && isInRange(end);
    }

    public Boolean isInRange(int number) {
        return number > 0 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i=start;i<=end;i++) {
            if (i == end) {
                stringBuilder.append(generateLine(start, i));
            } else {
                stringBuilder.append(generateLine(start, i)).append("%n");
            }
        }
        return String.format(stringBuilder.toString());
    }

    public String generateLine(int start, int row) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = start;i<=row;i++) {
            if (i == row) {
                stringBuilder.append(generateSingleExpression(i, row));
            } else {
                stringBuilder.append(generateSingleExpression(i, row)).append("  ");
            }
        }
        return stringBuilder.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return multiplicand + "*" + multiplier + "=" + multiplicand * multiplier;
    }
}
